# hexnocular-ofx

Projection (mapping) application for openFrameworks that is based on ofxPiMapper and also runs on the Raspberry Pi.

## Installation

### openFrameworks

Depending on your distribution you might either use the system-wide installation or a user-local clone of the repository.

### ofxPiMapper

```
cd ${OF_ROOT}/addons
git clone https://github.com/kr15h/ofxPiMapper.git
```

## License

