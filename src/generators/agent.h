#pragma once

#include <Algorithms.h>
#include "ofVec2f.h"

struct agent 
{
    agent(const float width, const float height)
        : current_(algorithms::generate_number<int>(0, width), 
                    algorithms::generate_number<int>(0, height))
        , old_(0, 0)
        , step_size_(algorithms::generate_number<int>(1, 5))
        , is_outside_(false) {}

private:
    ofVec2f current_;
    ofVec2f old_;
    float   step_size_;
    bool    is_outside_;
};