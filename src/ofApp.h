#pragma once

#include <ofMain.h>
#include <Settings.h>
#include <ofxPiMapper.h>
#include <LissajousSource.h>
#include <NeonSource.h>
#include <VideoSource.h>

class ofApp : public ofBaseApp {

public:

    void setup();
    void update();
    void draw();

    ofxPiMapper piMapper;

    std::unique_ptr<LissajousSource>    lissajousSource;
    std::unique_ptr<NeonSource>         neonSource;
};
