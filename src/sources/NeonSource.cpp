#include <NeonSource.h>

#include <agent.h>

#include <random>

NeonSource::NeonSource(const int width, const int height) : 
    FboSource(width, height)
{   
    name = "Neon";
}

void NeonSource::update()
{
    fbo_.begin();
    ofClear(0, 0, 0,255);
    // camera_.begin();

    agent a{width_, height_};
    
    ofTranslate(width_ / 2, height_ / 2, 0);

    for (int i = 0;i < 100;i++)
    {
        if (i % 5 == 0)
            ofSetColor(50 , 255, 100);
        else if (i % 9 == 0)
            ofSetColor(255, 50, 100);
        else
            ofSetColor(255, 255, 255);
        
        ofPushMatrix();
        ofRotate(ofGetFrameNum(), 1.0, 1.0, 1.0);
        ofTranslate((ofNoise(i/2.4)-0.5)*1000,
                    (ofNoise(i/5.6)-0.5)*1000,
                    (ofNoise(i/8.2)-0.5)*1000);
        ofCircle(0, 0, (ofNoise(i/3.4)-0.5)*100+ofRandom(3));
        ofPopMatrix();
    }
        
    // camera_.end();
    fbo_.end();
}